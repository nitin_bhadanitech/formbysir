import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from "@angular/forms";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form1',
  templateUrl: './form1.component.html',
  styleUrls: ['./form1.component.css']
})
export class Form1Component implements OnInit {

  form1: FormGroup
  isSubmitted = false
  // isDirty = false

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) {
    this.createForm1()
   }

  ngOnInit() {
    if(this.activatedRoute.snapshot.paramMap.get('userId')){
      this.getUserById(this.activatedRoute.snapshot.paramMap.get('userId'));
    }
  }

  createForm1(){
    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.form1 = this.fb.group({
      userId: [''],
      name: ['', [Validators.required, Validators.maxLength(10)]],
      email: ['', [Validators.required, Validators.pattern(emailregex)]],
      age: ['', [Validators.required]],
      phone: ['', [Validators.required, Validators.minLength(10)]],
      gender: ['', [Validators.required]],
      country: ['', [Validators.required]],
      state: ['', [Validators.required]],
      dateOfBirth : [''],
    })
  }
  values(){
    let userList = (localStorage.getItem('formvalue'))
    return userList;

  }

  get f(){
    return this.form1.controls
  }

  onSubmit(){
    console.log('form', this.form1)
    // console.log('form value', this.form1.value)

    this.isSubmitted = true
    if(this.form1.invalid){
      // alert("From is invalid")
      return
    }
    let userList = []
    if(localStorage.getItem('formvalue')){
      userList = JSON.parse(localStorage.getItem('formvalue'))

      this.form1.value.userId = this.generateUserId(userList.length)
      userList.push(this.form1.value)
    }else{
      this.form1.value.userId = this.generateUserId(userList.length)
      userList.push(this.form1.value)
    }
    localStorage.setItem('formvalue', JSON.stringify(userList))
    this.form1.reset()
    this.isSubmitted = false
    
  }

  generateUserId(id){
    return 'USER-' + (id + 1)
  }

  patchValue(){
    let formData = JSON.parse(localStorage.getItem('formvalue'))
    if(formData){
      this.form1.patchValue(formData)
    }else{
      alert('No data found')
    }
  }

  resetForm(){
    this.isSubmitted = false
    this.form1.reset()
  }

  numberOnly(event) {
    var k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return (k > 47 && k < 58) || k == 8 || k == 32;
  }

  OnCountryChange(val){
    if(val === 'bb'){
      this.form1.get('state').clearValidators()
      this.form1.get('state').updateValueAndValidity()
    }else{
      this.form1.get('state').setValidators([Validators.required])
      this.form1.get('state').updateValueAndValidity()
    }
  }

  getUserById(id){
    if(localStorage.getItem('formvalue')){
      let userList = JSON.parse(localStorage.getItem('formvalue'))
      let user = userList.find(x => x.userId == id)
      if(user){
        this.form1.patchValue(user)
      }
    }
  }
  

}
