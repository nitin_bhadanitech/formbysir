import { Component, OnInit } from '@angular/core';
import { comments } from '../classes/comments';
import { freeApiService } from '../services/freeapi.service';
import { Posts } from '../classes/posts';
@Component({
  selector: 'app-form2',
  templateUrl: './form2.component.html',
  styleUrls: ['./form2.component.css']
})
export class Form2Component implements OnInit {

  constructor( private _freeApiServices: freeApiService) {}
  lstComments: comments[];
  lstPosts: Posts[];
  objPosts: Posts[];
  objPuts: Posts[];

  // startIndex =0;
  // endIndex = 2;
  ngOnInit(){
    this._freeApiServices.getcomments()
    .subscribe(
        data=>{
            this.lstComments=data;
        }
    );
    this._freeApiServices.getCommentsByParameter()
    .subscribe(
      data=>{
        this.lstPosts=data;
    } 
    );
  let opost= new Posts();
  opost.body="testbody";
  opost.title= "testtitle";
  opost.userId= 5;
  

  this._freeApiServices.post(opost)
    .subscribe(
      data=>{
        this.objPosts=data;
    } 
    );
    this._freeApiServices.post(opost)
    .subscribe(
      data=>{
        this.objPosts=data;
    } 
    );
     opost =new Posts();
     opost.body="updating the body";
     opost.title="Updating the title";
     opost.userId=5;
     
     this._freeApiServices.put(opost)
     .subscribe(
       data=>{
         this.objPuts=data;
     } 
     );

  
  }

}
